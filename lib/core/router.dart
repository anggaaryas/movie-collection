import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';

import '../movie/domain/entities/movie.dart';
import '../movie/presentation/pages/detail_screen.dart';
import '../movie/presentation/pages/list_screen.dart';

part 'router.gr.dart';

@AutoRouterConfig()
class AppRouter extends _$AppRouter {

  @override
  List<AutoRoute> get routes => [
    AutoRoute(page: ListRoute.page, path: '/'),
    AutoRoute(page: DetailRoute.page)
  ];
} 