import 'package:auto_route/annotations.dart';
import 'package:auto_route/auto_route.dart';
import 'package:briix_angga_arya_saputra/core/di/di.dart';
import 'package:briix_angga_arya_saputra/core/router.dart';
import 'package:briix_angga_arya_saputra/movie/domain/entities/movie.dart';
import 'package:briix_angga_arya_saputra/movie/presentation/manager/movie_manager.dart';
import 'package:briix_angga_arya_saputra/movie/presentation/pages/detail_screen.dart';
import 'package:briix_angga_arya_saputra/movie/presentation/widgets/movie_list_item_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';

@RoutePage()
class ListScreen extends StatefulWidget{
  const ListScreen({super.key});

  @override
  State<ListScreen> createState() => _ListScreenState();
}

class _ListScreenState extends State<ListScreen> {

  final MovieManager manager = getIt();

  @override
  void initState() {
    super.initState();
    manager.retrieve();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:  Text("Movie Collection", style: TextStyle(fontSize: 32.spMin, fontWeight: FontWeight.bold),),
        centerTitle: true,
          surfaceTintColor: Colors.transparent
      ),
      body: Column(
        children: [
          Container(
            padding: const EdgeInsets.all(32),
            decoration:  BoxDecoration(
              borderRadius: BorderRadius.vertical(bottom: Radius.circular(16)),
              boxShadow: [
                BoxShadow(
                  blurRadius: 100,
                  color: Colors.black38.withOpacity(0.3)
                )
              ],
              color: Colors.white
            ),
            child: TextField(
              onChanged: (value){
                manager.search(value);
              },
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                hintText: "Search by title",
                prefixIcon: Icon(Icons.search)
              ),
            ),
          ),
          Expanded(
            child: Observer(
              builder: (context) => manager.movies.isEmpty? Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                      width: 128.r,
                      height: 128.r,
                      child: kIsWeb? SvgPicture.network("https://www.anggaaryas.my.id/demo-movie-collection/assets/assets/movie.svg", color: Colors.deepPurpleAccent,)
                          : SvgPicture.asset( "assets/movie.svg", color: Colors.deepPurpleAccent,)),
                  Text("The Movie is Empty", style: TextStyle(fontSize: 24.spMin),)
                ],
              ):GridView(
                padding: const EdgeInsets.symmetric(horizontal: 32, vertical: 64),
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 1.sw ~/ 512 > 0? 1.sw ~/ 512 : 1,
                    childAspectRatio: 16/9
                ),
                children: manager.movies.map((movie) => GestureDetector(
                    onTap: (){
                      context.router.push(DetailRoute(canDelete: true, initialData: movie));
                    },
                    child: MovieListItem(movie: movie))).toList(),
              ),
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton.large(
        onPressed: (){
          context.router.push(DetailRoute());
        },
        backgroundColor: Colors.deepPurpleAccent,
        child:  Container(
          width: 48.r,
          height: 48.r,
          child: kIsWeb? SvgPicture.network("https://www.anggaaryas.my.id/demo-movie-collection/assets/assets/add.svg", color: Colors.white,) : SvgPicture.asset(
            "assets/add.svg", color: Colors.white,
          ),
        ),
      ),
    );
  }
}