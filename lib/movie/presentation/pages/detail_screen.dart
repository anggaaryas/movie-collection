import 'package:auto_route/annotations.dart';
import 'package:auto_route/auto_route.dart';
import 'package:briix_angga_arya_saputra/core/di/di.dart';
import 'package:briix_angga_arya_saputra/movie/presentation/manager/movie_manager.dart';
import 'package:briix_angga_arya_saputra/movie/presentation/widgets/genre_chip_widget.dart';
import 'package:briix_angga_arya_saputra/movie/presentation/widgets/text_button_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../domain/entities/genre.dart';
import '../../domain/entities/movie.dart';

@RoutePage()
class DetailScreen extends StatefulWidget {
  const DetailScreen(
      {super.key,
      this.canSave = true,
      this.canDelete = false,
      this.initialData});

  final bool canSave;
  final bool canDelete;
  final Movie? initialData;

  @override
  State<DetailScreen> createState() => _DetailScreenState();
}

class _DetailScreenState extends State<DetailScreen> {
  final _formKey = GlobalKey<FormState>();

  final MovieManager movieManager = getIt();

  final TextEditingController titleTf = TextEditingController();
  final TextEditingController directorTf = TextEditingController();
  final TextEditingController summaryTf = TextEditingController();

  final List<Genre> genres = [];

  @override
  void initState() {
    if (widget.initialData != null) {
      titleTf.text = widget.initialData!.title;
      directorTf.text = widget.initialData!.director;
      summaryTf.text = widget.initialData!.summary;
      genres.addAll(widget.initialData!.genres.toListGenre());
    }
    super.initState();
  }

  @override
  void dispose() {
    titleTf.dispose();
    directorTf.dispose();
    summaryTf.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(32),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  widget.canDelete
                      ? TextButtonWidget(
                          name: "Delete",
                          onPressed: onDelete,
                          negtive: true,
                        )
                      : Container(),
                  widget.canSave
                      ? TextButtonWidget(name: "Save", onPressed: onSaveMovie)
                      : Container(),
                ],
              ),
              const SizedBox(
                height: 32,
              ),
              MovieFormWidget(
                  formKey: _formKey,
                  titleTf: titleTf,
                  directorTf: directorTf,
                  summaryTf: summaryTf,
                  genres: genres)
            ],
          ),
        ),
      ),
    );
  }

  onDelete() {
    movieManager.delete(widget.initialData!);
    context.router.back();
  }

  onSaveMovie() {
    if (_formKey.currentState!.validate()) {
      movieManager.save(
          title: titleTf.text,
          director: directorTf.text,
          genres: genres,
          id: widget.initialData?.id,
          summary: summaryTf.text);
      context.router.back();
    }
  }
}

class MovieFormWidget extends StatelessWidget {
  const MovieFormWidget({
    super.key,
    required GlobalKey<FormState> formKey,
    required this.titleTf,
    required this.directorTf,
    required this.summaryTf,
    required this.genres,
  }) : _formKey = formKey;

  final GlobalKey<FormState> _formKey;
  final TextEditingController titleTf;
  final TextEditingController directorTf;
  final TextEditingController summaryTf;
  final List<Genre> genres;

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child: Column(
          children: [
            TextFormField(
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return "Title can not be empty";
                }

                return null;
              },
              controller: titleTf,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Title',
              ),
            ),
            const SizedBox(
              height: 32,
            ),
            TextFormField(
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return "Director can not be empty";
                }

                return null;
              },
              controller: directorTf,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Director',
              ),
            ),
            const SizedBox(
              height: 32,
            ),
            TextFormField(
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return "Summary can not be empty";
                }

                return null;
              },
              controller: summaryTf,
              maxLength: 100,
              maxLines: 5,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Summary',
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            Container(
              width: double.infinity,
              child: Wrap(
                alignment: WrapAlignment.start,
                spacing: 12,
                children: Genre.values
                    .map((e) => GenreChipWidget(
                          genre: e.value,
                          enabled: genres.contains(e),
                          onPressed: (isEnable) {
                            if (isEnable) {
                              genres.add(e);
                            } else {
                              genres.remove(e);
                            }
                          },
                        ))
                    .toList(),
              ),
            )
          ],
        ));
  }
}
