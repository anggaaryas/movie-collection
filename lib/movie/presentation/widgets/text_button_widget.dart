import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class TextButtonWidget extends StatelessWidget {
  const TextButtonWidget(
      {super.key, required this.name, required this.onPressed, this.negtive = false});

  final String name;
  final Function() onPressed;
  final bool negtive;

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: onPressed,
      child: Text(
        name,
        style: TextStyle(fontSize: 24.spMin, color: negtive? Colors.redAccent : Colors.greenAccent),
      ),
    );
  }
}
