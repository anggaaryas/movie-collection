import 'package:briix_angga_arya_saputra/movie/presentation/widgets/genre_chip_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../domain/entities/movie.dart';

class MovieListItem extends StatelessWidget{
  const MovieListItem({super.key, required this.movie});

  final Movie movie;

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.symmetric(horizontal: 32, vertical: 16),
      child: AspectRatio(
        aspectRatio: 16/9,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(movie.title, style: TextStyle(fontSize: 24.spMin, fontWeight: FontWeight.bold),),
              Text(movie.director, style: TextStyle(fontSize: 24.spMin)),
              Spacer(),
              Container(
                width: double.infinity,
                child: Wrap(
                  spacing: 12,
                  runSpacing: 12,
                  alignment: WrapAlignment.end,
                  children: movie.genres.map((genre) => GenreChipWidget(genre: genre)).toList(),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

}