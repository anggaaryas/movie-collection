import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class GenreChipWidget extends StatefulWidget{
  const GenreChipWidget({super.key, required this.genre, this.enabled = false, this.onPressed});

  final String genre;
  final bool enabled;
  final Function(bool)? onPressed;

  @override
  State<GenreChipWidget> createState() => _GenreChipWidgetState();
}

class _GenreChipWidgetState extends State<GenreChipWidget> {
  ValueNotifier<bool> enabled = ValueNotifier(false);

  @override
  void initState() {
    enabled.value = widget.enabled;
    super.initState();
  }

  @override
  void dispose() {
    enabled.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<bool>(
      valueListenable: enabled,
      builder: (context, isEnable, _) => GestureDetector(
        onTap: widget.onPressed == null ? null : (){
          enabled.value = !enabled.value;
          widget.onPressed!(enabled.value);
        },
        child: Chip(
            label: Text(widget.genre, style: TextStyle(fontSize: 16.spMin, color: isEnable? Colors.white: null),),
          backgroundColor: isEnable? Colors.purple : Colors.white,
        ),
      ),
    );
  }
}