// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie_manager.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$MovieManager on MovieManagerBase, Store {
  late final _$moviesAtom =
      Atom(name: 'MovieManagerBase.movies', context: context);

  @override
  List<Movie> get movies {
    _$moviesAtom.reportRead();
    return super.movies;
  }

  @override
  set movies(List<Movie> value) {
    _$moviesAtom.reportWrite(value, super.movies, () {
      super.movies = value;
    });
  }

  late final _$searchKeywordAtom =
      Atom(name: 'MovieManagerBase.searchKeyword', context: context);

  @override
  String? get searchKeyword {
    _$searchKeywordAtom.reportRead();
    return super.searchKeyword;
  }

  @override
  set searchKeyword(String? value) {
    _$searchKeywordAtom.reportWrite(value, super.searchKeyword, () {
      super.searchKeyword = value;
    });
  }

  late final _$errorAtom =
      Atom(name: 'MovieManagerBase.error', context: context);

  @override
  AppException? get error {
    _$errorAtom.reportRead();
    return super.error;
  }

  @override
  set error(AppException? value) {
    _$errorAtom.reportWrite(value, super.error, () {
      super.error = value;
    });
  }

  late final _$saveAsyncAction =
      AsyncAction('MovieManagerBase.save', context: context);

  @override
  Future<void> save(
      {required String title,
      required String director,
      required List<Genre> genres,
      required String summary,
      String? id}) {
    return _$saveAsyncAction.run(() => super.save(
        title: title,
        director: director,
        genres: genres,
        summary: summary,
        id: id));
  }

  late final _$deleteAsyncAction =
      AsyncAction('MovieManagerBase.delete', context: context);

  @override
  Future<void> delete(Movie movie) {
    return _$deleteAsyncAction.run(() => super.delete(movie));
  }

  late final _$retrieveAsyncAction =
      AsyncAction('MovieManagerBase.retrieve', context: context);

  @override
  Future<void> retrieve([String? name]) {
    return _$retrieveAsyncAction.run(() => super.retrieve(name));
  }

  late final _$searchAsyncAction =
      AsyncAction('MovieManagerBase.search', context: context);

  @override
  Future<void> search(String name) {
    return _$searchAsyncAction.run(() => super.search(name));
  }

  late final _$getFilteredDataAsyncAction =
      AsyncAction('MovieManagerBase.getFilteredData', context: context);

  @override
  Future<void> getFilteredData() {
    return _$getFilteredDataAsyncAction.run(() => super.getFilteredData());
  }

  @override
  String toString() {
    return '''
movies: ${movies},
searchKeyword: ${searchKeyword},
error: ${error}
    ''';
  }
}
