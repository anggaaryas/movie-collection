import 'package:briix_angga_arya_saputra/core/app_exception.dart';
import 'package:briix_angga_arya_saputra/movie/domain/entities/delete_movie_param.dart';
import 'package:briix_angga_arya_saputra/movie/domain/entities/get_all_movie_param.dart';
import 'package:briix_angga_arya_saputra/movie/domain/entities/no_param.dart';
import 'package:briix_angga_arya_saputra/movie/domain/entities/save_movie_param.dart';
import 'package:briix_angga_arya_saputra/movie/domain/repositories/movie_repository.dart';
import 'package:briix_angga_arya_saputra/movie/domain/use_cases/delete_movie.dart';
import 'package:briix_angga_arya_saputra/movie/domain/use_cases/get_all_movie.dart';
import 'package:briix_angga_arya_saputra/movie/domain/use_cases/save_movie.dart';
import 'package:injectable/injectable.dart';
import 'package:mobx/mobx.dart';

import '../../domain/entities/genre.dart';
import '../../domain/entities/movie.dart';

part 'movie_manager.g.dart';

@Singleton()
class MovieManager = MovieManagerBase with _$MovieManager;

abstract class MovieManagerBase with Store{

  DeleteMovie deleteMovieUseCase = DeleteMovie();
  GetAllMovie getAllMovieUseCase = GetAllMovie();
  SaveMovie saveMovieUseCase = SaveMovie();

  @observable
  List<Movie> movies = [];

  @observable
  String? searchKeyword;

  @observable
  AppException? error;

  @action
  Future<void> save({required String title, required String director, required List<Genre> genres, required String summary, String? id}) async {
    final (err, newData) = await saveMovieUseCase(SaveMovieParam(title: title, director: director, genres: genres, summary: summary, id: id));
    if(err == null){
      getFilteredData();
      error = null;
    } else {
      error = err;
      movies = [];
    }
  }

  @action
  Future<void> delete(Movie movie) async {
    final (err, newData) = await deleteMovieUseCase(DeleteMovieParam(movie: movie));
    if(err == null){
      getFilteredData();
      error = null;
    } else {
      error = err;
      movies = [];
    }
  }

  @action
  Future<void> retrieve([String? name]) async {
    final (err, newData) = await getAllMovieUseCase(GetAllMovieParam(title: name));
    if(err == null){
      movies = newData!;
      error = null;
    } else {
      error = err;
      movies = [];
    }
  }

  @action
  Future<void> search(String name) async {
    searchKeyword = name;
    await retrieve(name);
  }

  @action
  Future<void> getFilteredData() async {
    await retrieve(searchKeyword);
  }


}