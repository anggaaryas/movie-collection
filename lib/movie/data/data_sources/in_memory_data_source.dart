import 'package:briix_angga_arya_saputra/movie/domain/entities/movie.dart';

class InMemoryDataSource {
  List<Movie> data = [];

  List<Movie> save(Movie movie) {
    if (data.contains(movie)) {
      data.remove(movie);
    }

    data.add(movie);
    return List.from(data);
  }

  List<Movie> delete(Movie movie) {
    data.remove(movie);
    return List.from(data);
  }

  List<Movie> get([String? name]) => name == null
      ? List.from(data)
      : List.from(data.where((element) => _isStartWith(name, element.title)));

  bool _isStartWith(String word, String search){
    RegExp regex = RegExp(r'^' + word.toLowerCase());
    final isValidWord = regex.hasMatch(search.toLowerCase());
    return isValidWord;
  }
}
