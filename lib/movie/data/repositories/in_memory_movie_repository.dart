import 'package:briix_angga_arya_saputra/core/app_exception.dart';
import 'package:briix_angga_arya_saputra/movie/data/data_sources/in_memory_data_source.dart';

import 'package:briix_angga_arya_saputra/movie/domain/entities/movie.dart';
import 'package:injectable/injectable.dart';

import '../../domain/repositories/movie_repository.dart';

@Singleton(as: MovieRepository, order: -1)
class InMemoryMovieRepository implements MovieRepository{
  final InMemoryDataSource dataSource = InMemoryDataSource();

  @override
  Future<(AppException?, List<Movie>?)> delete(Movie movie) async {
    try{
      return (null, dataSource.delete(movie));
    } catch(e){
      return (AppException(e.toString()), null);
    }
  }

  @override
  Future<(AppException?, List<Movie>?)> get([String? name]) async {
    try{
      return (null, dataSource.get(name));
    } catch(e){
      return (AppException(e.toString()), null);
    }
  }

  @override
  Future<(AppException?, List<Movie>?)> save(Movie movie) async {
    try{
      return (null, dataSource.save(movie));
    } catch(e){
      return (AppException(e.toString()), null);
    }
  }
  
}