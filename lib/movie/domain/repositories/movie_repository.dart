import 'package:briix_angga_arya_saputra/core/app_exception.dart';
import 'package:briix_angga_arya_saputra/movie/domain/entities/movie.dart';

abstract class MovieRepository{
  Future<(AppException?, List<Movie>?)> save(Movie movie, );
  Future<(AppException?, List<Movie>?)> get([String? name]);
  Future<(AppException?, List<Movie>?)> delete(Movie movie);
}