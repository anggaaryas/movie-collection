import 'package:briix_angga_arya_saputra/core/app_exception.dart';
import 'package:briix_angga_arya_saputra/core/di/di.dart';
import 'package:briix_angga_arya_saputra/core/uuid.dart';
import 'package:briix_angga_arya_saputra/movie/domain/entities/genre.dart';
import 'package:briix_angga_arya_saputra/movie/domain/entities/save_movie_param.dart';
import 'package:briix_angga_arya_saputra/movie/domain/repositories/movie_repository.dart';

import '../entities/movie.dart';

class SaveMovie {
  MovieRepository repository = getIt();

  Future<(AppException?, List<Movie>?)> call(SaveMovieParam param) {
    return repository.save(Movie(param.id ?? uuid.v4(), param.title, param.director, param.summary, param.genres.toListString()));
  }
}
