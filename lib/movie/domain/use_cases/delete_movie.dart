import 'package:briix_angga_arya_saputra/core/app_exception.dart';
import 'package:briix_angga_arya_saputra/core/di/di.dart';
import 'package:briix_angga_arya_saputra/movie/domain/entities/delete_movie_param.dart';
import 'package:briix_angga_arya_saputra/movie/domain/repositories/movie_repository.dart';

import '../entities/movie.dart';

class DeleteMovie{
  MovieRepository repository = getIt();

  Future<(AppException?, List<Movie>?)> call(DeleteMovieParam param){
    return repository.delete(param.movie);
  }
}