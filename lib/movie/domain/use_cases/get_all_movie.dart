import 'package:briix_angga_arya_saputra/core/app_exception.dart';
import 'package:briix_angga_arya_saputra/movie/domain/entities/get_all_movie_param.dart';
import 'package:briix_angga_arya_saputra/movie/domain/entities/movie.dart';
import 'package:briix_angga_arya_saputra/movie/domain/entities/no_param.dart';
import 'package:briix_angga_arya_saputra/movie/domain/repositories/movie_repository.dart';

import '../../../core/di/di.dart';

class GetAllMovie{
  MovieRepository repository = getIt();

  Future<(AppException?, List<Movie>?)> call(GetAllMovieParam param){
    return repository.get(param.title);
  }
}