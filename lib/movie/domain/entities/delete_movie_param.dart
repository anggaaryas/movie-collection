import 'movie.dart';

class DeleteMovieParam{
  final Movie movie;

  DeleteMovieParam({required this.movie});
}