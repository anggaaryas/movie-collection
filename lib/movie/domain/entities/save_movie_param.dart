import 'package:briix_angga_arya_saputra/movie/domain/entities/genre.dart';

class SaveMovieParam{
  final String title;
  final String director;
  final String summary;
  final List<Genre> genres;
  final String? id;


  SaveMovieParam({required this.title, required this.director, required this.genres, required this.summary, this.id});
}