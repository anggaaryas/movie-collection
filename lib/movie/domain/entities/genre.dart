enum Genre{
  drama("Drama"),
  action("Action"),
  animation("Animation"),
  sciFi("Sci-Fi"),
  horror("Horror");

  const Genre(this.value);
  final String value;
}

extension ToListString on List<Genre>{
  List<String> toListString(){
    List<String> result = [];
    for(Genre genre in this){
      result.add(genre.value);
    }

    return result;
  }
}

extension ToListGenre on List<String>{
  List<Genre> toListGenre(){
    List<Genre> result = [];
    for(String genre in this){
      result.add(Genre.values.firstWhere((element) => element.value == genre));
    }

    return result;
  }
}